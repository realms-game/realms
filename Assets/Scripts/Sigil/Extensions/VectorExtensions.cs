﻿using UnityEngine;

namespace Sigil.Extensions {
    public static class Vector3Extensions {
        public static Vector3Int Vector3Int(this Vector3 vec3) {
            return new Vector3Int((int)vec3.x, (int)vec3.y, (int)vec3.z);
        }
    }
}