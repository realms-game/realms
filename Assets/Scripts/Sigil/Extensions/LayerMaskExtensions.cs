using UnityEngine;

namespace Sigil.Extensions {
    public static class LayerMaskExtensions {
        public static bool Contains(this LayerMask layerMask, int layerInt) {
            return (layerMask | (1 << layerInt)) == layerMask;
        }
    }
}