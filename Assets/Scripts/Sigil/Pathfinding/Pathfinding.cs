﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Sigil.Data;
using UnityEngine;

namespace Sigil.Pathfinding {
    public class Pathfinding : MonoBehaviour {
        private PathRequestManager _requestManager;
        private PathfindingGrid _grid;

        private void Awake() {
            _requestManager = GetComponent<PathRequestManager>();
            _grid = GetComponent<PathfindingGrid>();
        }

        public void StartFindPath(Vector3 startPos, Vector3 targetPos) {
            StartCoroutine(FindPath(startPos, targetPos));
        }

        private IEnumerator FindPath(Vector3 startPos, Vector3 targetPos) {
            var waypoints = new Vector3[0];
            var pathSuccess = false;

            var startNode = _grid.NodeFromWorldPoint(startPos);
            var targetNode = _grid.NodeFromWorldPoint(targetPos);

            if (startNode.Walkable && targetNode.Walkable) {
                var openSet = new Heap<PathfindingGridNode>(_grid.NodeCount);
                var closedSet = new HashSet<PathfindingGridNode>();
                openSet.Add(startNode);

                while (openSet.Count > 0) {
                    var currentNode = openSet.RemoveFirst();
                    closedSet.Add(currentNode);

                    if (currentNode == targetNode) {
                        pathSuccess = true;
                        break;
                    }

                    foreach (var neighbour in _grid.GetNeighboursOfNode(currentNode)) {
                        if (!neighbour.Walkable || closedSet.Contains(neighbour)) {
                            continue;
                        }

                        var newMovementCostToNeighbour = currentNode.GCost + GetDistance(currentNode, neighbour);
                        if (newMovementCostToNeighbour >= neighbour.GCost && openSet.Contains(neighbour)) continue;
                        
                        neighbour.GCost = newMovementCostToNeighbour;
                        neighbour.HCost = GetDistance(neighbour, targetNode);
                        neighbour.ParentNode = currentNode;

                        if (!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                    }
                }
            }

            yield return null;
            if (pathSuccess) {
                waypoints = RetracePath(startNode, targetNode);
            }

            _requestManager.FinishedProcessingPath(waypoints, pathSuccess);
        }

        private static Vector3[] RetracePath(PathfindingGridNode startNode, PathfindingGridNode endNode) {
            var path = new List<PathfindingGridNode>();
            var currentNode = endNode;

            while (currentNode != startNode) {
                path.Add(currentNode);
                currentNode = currentNode.ParentNode;
            }

            return path.Select(n => n.WorldPosition).Reverse().ToArray();
        }

        private static int GetDistance(PathfindingGridNode nodeA, PathfindingGridNode nodeB) {
            var dstX = Mathf.Abs(nodeA.GridPosition.x - nodeB.GridPosition.x);
            var dstY = Mathf.Abs(nodeA.GridPosition.y - nodeB.GridPosition.y);

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }
    }
}