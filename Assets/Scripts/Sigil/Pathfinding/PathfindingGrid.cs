﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Sigil.Data;
using Sigil.Extensions;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Sigil.Pathfinding {
    public class PathfindingGrid : MonoBehaviour {
        // Grid containing our game tilemaps
        public Grid Grid;

        // The layer in which we have our blocking objects
        public LayerMask WalkableMask;

        // The layer in which we have our blocking objects
        public LayerMask BlockingMask;

        // The walkable tilemaps
        private readonly List<Tilemap> _walkableTilemaps = new List<Tilemap>();

        // The blocking tilemaps
        private readonly List<Tilemap> _blockingTilemaps = new List<Tilemap>();

        // All the nodes used in pathfinding
        private PathfindingGridNode[,] _nodes;

        // Total amount of nodes
        public int NodeCount => _nodes.Length;

        private void Start() {
            LoadTilemaps();
            CreateNodes();
        }

        // Load all the tilemaps and organize them in the correct lists
        private void LoadTilemaps() {
            var tilemaps = Grid.GetComponentsInChildren<Tilemap>();
            foreach (var tilemap in tilemaps) {
                if (BlockingMask.Contains(tilemap.gameObject.layer)) {
                    _blockingTilemaps.Add(tilemap);
                }

                if (WalkableMask.Contains(tilemap.gameObject.layer)) {
                    _walkableTilemaps.Add(tilemap);
                }
            }
        }

        // Create the nodes array for pathfinding
        private void CreateNodes() {
            // Calculate total area of walkable tilemaps
            float minX = int.MaxValue;
            float minY = int.MaxValue;
            float maxX = int.MinValue;
            float maxY = int.MinValue;
            foreach (var tilemap in _walkableTilemaps) {
                var bounds = tilemap.localBounds;
                minX = Mathf.Min(minX, bounds.min.x);
                minY = Mathf.Min(minY, bounds.min.y);
                maxX = Mathf.Max(maxX, bounds.min.x + bounds.size.x);
                maxY = Mathf.Max(maxY, bounds.min.y + bounds.size.y);
            }

            // Generate nodes array
            var gridCols = (int)(Mathf.Abs(minX) + Mathf.Abs(maxX));
            var gridRows = (int)(Mathf.Abs(minY) + Mathf.Abs(maxY));
            _nodes = new PathfindingGridNode[gridCols, gridRows];
            for (var x = 0; x < gridCols; x++) {
                for (var y = 0; y < gridRows; y++) {
                    _nodes[x, y] = new PathfindingGridNode(
                        _blockingTilemaps.Any(t => t.GetTile(new Vector3Int(x, y, 0)) == null),
                        Grid.GetCellCenterWorld(new Vector3(minX + x, minY + y).Vector3Int()),
                        new Vector2Int(x, y)
                    );
                }
            }
        }

        public List<PathfindingGridNode> GetNeighboursOfNode(PathfindingGridNode node) {
            var neighbours = new List<PathfindingGridNode>();
            
            for (var x = -1; x <= 1; x++) {
                for (var y = -1; y <= 1; y++) {
                    // Skip self
                    if (
                        (x == 0 && y == 0)
                        || (x == -1 && y == -1) // top left
                        || (x == -1 && y == 1) // bottom left
                        || (x == 1 && y == 1) // bottom right
                        || (x == 1 && y == -1)) // top right
                        continue;

                    var checkX = node.GridPosition.x + x;
                    var checkY = node.GridPosition.y + y;

                    if (checkX >= 0 && checkX < _nodes.GetLength(0) && checkY >= 0 && checkY < _nodes.GetLength(1)) {
                        neighbours.Add(_nodes[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }

        public PathfindingGridNode NodeFromWorldPoint(Vector3 worldPosition) {
            var cell = Grid.LocalToCell(worldPosition);
            return _nodes[cell.x, cell.y];
        }

        private void OnDrawGizmos() {
            if (_nodes == null || _nodes.Length == 0) return;

            foreach (var node in _nodes) {
                Gizmos.color = node.Walkable ? Color.white : Color.red;
                Gizmos.DrawWireCube(Grid.GetCellCenterWorld(node.WorldPosition.Vector3Int()), Vector3.one);
            }
        }
    }
}