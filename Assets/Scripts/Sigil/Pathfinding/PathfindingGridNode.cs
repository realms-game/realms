﻿using Sigil.Data;
using UnityEngine;

namespace Sigil.Pathfinding {

    public class PathfindingGridNode : IHeapItem<PathfindingGridNode> {
        public bool Walkable;
        public Vector3 WorldPosition;
        public Vector2Int GridPosition;

        public int GCost;
        public int HCost;
        public PathfindingGridNode ParentNode;

        public int FCost => GCost + HCost;
        public int HeapIndex { get; set; }

        public PathfindingGridNode(bool walkable, Vector3 worldPosition, Vector2Int gridPosition) {
            Walkable = walkable;
            WorldPosition = worldPosition;
            GridPosition = gridPosition;
        }

        public int CompareTo(PathfindingGridNode other) {
            var compare = FCost.CompareTo(other.FCost);
            if (compare == 0)
                compare = HCost.CompareTo(other.HCost);
            return -compare;
        }
    }

}