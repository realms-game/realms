﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Sigil.Utilities {
    public static class ScriptableObjectUtility {
        public static void CreateAsset<T>() where T : ScriptableObject {
            var asset = ScriptableObject.CreateInstance<T>();

            var path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == "") {
                path = "Assets";
            }
            else if (Path.GetExtension(path) != "") {
                path = path.Replace(Path.GetFileName(path), "");
            }

            path = path + "/New " + typeof(T).Name + ".asset";
            var uniquePath = AssetDatabase.GenerateUniqueAssetPath(path);

            AssetDatabase.CreateAsset(asset, uniquePath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
    }
}