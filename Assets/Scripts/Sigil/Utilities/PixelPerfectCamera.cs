﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Sigil.Utilities {
    [ExecuteInEditMode]
    public class PixelPerfectCamera : MonoBehaviour {
        [SerializeField] private int _pixelsPerUnit = 16;
        [SerializeField] private int _verticalUnitsOnScreen = 4;

        private Camera _camera;

        private void Awake() {
            _camera = gameObject.GetComponent<Camera>();
            Assert.IsNotNull(_camera);

            SetOrthographicSize();
        }

        private void SetOrthographicSize() {
            ValidateUserInput();

            // get device's screen height and divide by the number of units 
            // that we want to fit on the screen vertically. this gets us
            // the basic size of a unit on the the current device's screen.
            var tempUnitSize = Screen.height / _verticalUnitsOnScreen;

            // with a basic rough unit size in-hand, we now round it to the
            // nearest power of pixelsPerUnit (ex; 16px.) this will guarantee
            // our sprites are pixel perfect, as they can now be evenly divided
            // into our final device's screen height.
            var finalUnitSize = GetNearestMultiple(tempUnitSize, _pixelsPerUnit);

            // ultimately, we are using the standard pixel art formula for 
            // orthographic cameras, but approaching it from the view of:
            // how many standard Unity units do we want to fit on the screen?
            // formula: cameraSize = ScreenHeight / (DesiredSizeOfUnit * 2)
            _camera.orthographicSize = Screen.height / (finalUnitSize * 2.0f);
        }

        private static int GetNearestMultiple(int value, int multiple) {
            var rem = value % multiple;
            var result = value - rem;
            if (rem > multiple / 2)
                result += multiple;

            return result;
        }

        private void ValidateUserInput() {
            if (_pixelsPerUnit == 0) {
                _pixelsPerUnit = 1;
                Debug.Log("Warning: Pixels-per-unit must be greater than zero. " +
                          "Resetting to minimum allowed.");
            }
            else if (_verticalUnitsOnScreen == 0) {
                _verticalUnitsOnScreen = 1;
                Debug.Log("Warning: Units-on-screen must be greater than zero." +
                          "Resetting to minimum allowed.");
            }
        }
    }
}
