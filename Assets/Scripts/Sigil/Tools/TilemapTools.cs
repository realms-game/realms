using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Sigil.Tools {
    /// <summary>
    /// Tilemap Bounds Compression.
    /// </summary>
    public static partial class TilemapTools {
        [MenuItem("Sigil/Tilemaps/Compress Tilemap Bounds")]
        public static void CompressSelectedTilemapBounds() {
            var obj = Selection.activeGameObject;
            var map = obj.GetComponent<Tilemap>();
            if (map == null) {
                Debug.Log("No Tilemap found on selected GameObject.");
                return;
            }

            CompressTilemapBounds(map);
        }

        [MenuItem("Sigil/Tilemaps/Compress Tilemap Bounds", true)]
        public static bool ValidateCompressSelectedTilemapBounds() {
            return Selection.activeGameObject != null;
        }

        [MenuItem("Sigil/Tilemaps/Compress All Tilemap Bounds")]
        public static void CompressAllTilemapBounds() {
            Object.FindObjectsOfType<Tilemap>().ToList().ForEach(CompressTilemapBounds);
        }

        private static void CompressTilemapBounds(Tilemap map) {
            map.CompressBounds();
            Debug.Log("Compressed bounds for " + map.name + " tilemap.");
        }
    }
}