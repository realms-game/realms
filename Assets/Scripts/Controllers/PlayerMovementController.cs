﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using Sigil.Extensions;
using Sigil.Pathfinding;
using UnityEngine;

namespace Controllers {
    public class PlayerMovementController : MonoBehaviour {
        public int MovementSpeed;
        public Grid MovementGrid;

        private Animator _animator;
        private bool _isWalking;
        
        [SerializeField]
        private Vector3 _targetPosition;

        [SerializeField]
        private Vector3[] _movementPath;
        private int _targetIndex;

        private void Start() {
            _animator = GetComponent<Animator>();
            _targetPosition = MovementGrid.LocalToCell(transform.position);
            _targetPosition = MovementGrid.GetCellCenterWorld(_targetPosition.Vector3Int());
        }
        
        private void Update() {
            if (Input.GetMouseButton(0)) {
                var worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _targetPosition = MovementGrid.LocalToCell(worldPosition);
                _targetPosition = MovementGrid.GetCellCenterWorld(_targetPosition.Vector3Int());
                PathRequestManager.RequestPath(transform.position, _targetPosition, OnPathFound);
            }
        }

        public void OnPathFound(Vector3[] newPath, bool pathSuccessful) {
            if (!pathSuccessful) return;
            _movementPath = newPath;
            _targetIndex = 0;
            StopCoroutine(nameof(FollowPath));
            StartCoroutine(nameof(FollowPath));
        }

        private IEnumerator FollowPath() {
            if (_movementPath.Length == 0) {
                yield break;
            }

            var currentWaypoint = _movementPath[0];
            while (true) {
                if (transform.position == currentWaypoint) {
                    _targetIndex++;
                    if (_targetIndex >= _movementPath.Length) {
                        _animator.SetBool("isWalking", false);
                        _isWalking = false;
                        yield break;
                    }

                    currentWaypoint = _movementPath[_targetIndex];
                }

                _isWalking = true;
                _animator.SetBool("isWalking", _isWalking);

                var walkingDirection = -(transform.position - currentWaypoint).normalized;
                _animator.SetFloat("x", walkingDirection.x);
                _animator.SetFloat("y", walkingDirection.y);

                _animator.SetBool("isWalking", _isWalking);
                
                transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, MovementSpeed * Time.deltaTime);
                yield return null;
            }
        }

        public void OnDrawGizmos() {
            if (_movementPath == null) return;

            for (var i = _targetIndex; i < _movementPath.Length; i++) {
                Gizmos.color = Color.black;

                if (i == _targetIndex) {
                    Gizmos.DrawLine(transform.position, _movementPath[i]);
                }
                else {
                    Gizmos.DrawLine(_movementPath[i - 1], _movementPath[i]);
                }
            }
        }
    }
}